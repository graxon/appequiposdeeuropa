package com.gerberraxon.stadiuminfoapp.Adapters;


import android.content.Context;
import android.content.res.Resources;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.gerberraxon.stadiuminfoapp.R;
import com.gerberraxon.stadiuminfoapp.beans.Stadium;

import java.util.List;

/**
 * Created by graxon on 18/10/15.
 */
public class StadiumAdapter extends RecyclerView.Adapter<StadiumAdapter.ViewHolder> {
    private List<Stadium> itemsData;
    Typeface font;

    public StadiumAdapter(Context parentContext, List<Stadium> data) {
        this.itemsData = data;
        font = Typeface.createFromAsset(parentContext.getAssets(), "fonts/Caviar-Dreams/CaviarDreams.ttf");
    }

    @Override
    public StadiumAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                         int viewType) {
        View itemLayoutView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.stadium_list_item, parent, false);

        return new ViewHolder(itemLayoutView);
    }

    public void updateData(List<Stadium> items) {
        itemsData.clear();
        itemsData.addAll(items);
        notifyDataSetChanged();
    }
    public void addItem(int position, Stadium item) {
        itemsData.add(position, item);
        notifyItemInserted(position);
    }

    public void removeItem(int position) {
        itemsData.remove(position);
        notifyItemRemoved(position);
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int position) {
        String name = itemsData.get(position).getName();
        String presentation = (name.length() > 25)? name.substring(0, 22) + "...": name;

        viewHolder.tv_name.setText(presentation);
        viewHolder.tv_address.setTypeface(font);
        viewHolder.tv_address.setText(itemsData.get(position).getAddress());
    }

    public TextView tv_name;
    public TextView tv_address;

    class ViewHolder extends RecyclerView.ViewHolder {

        public TextView tv_name;
        public TextView tv_address;

        public ViewHolder(View itemLayoutView) {
            super(itemLayoutView);
            tv_name = (TextView) itemLayoutView.findViewById(R.id.tv_name);
            tv_address = (TextView) itemLayoutView.findViewById(R.id.tv_address);
        }
    }

    @Override
    public int getItemCount() {
        return itemsData.size();
    }
}