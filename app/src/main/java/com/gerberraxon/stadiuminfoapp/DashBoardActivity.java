package com.gerberraxon.stadiuminfoapp;

import android.content.Context;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.gerberraxon.stadiuminfoapp.Retrofit.StadiumService;
import com.gerberraxon.stadiuminfoapp.beans.Stadium;
import com.squareup.okhttp.OkHttpClient;

import java.util.List;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.OkClient;
import retrofit.client.Response;


public class DashBoardActivity extends ActionBarActivity implements ListView.OnItemClickListener{

    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    private List<Stadium> stadiums;
    com.gerberraxon.stadiuminfoapp.Adapters.StadiumAdapter stdAdtr;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dash_board);

        RestAdapter.Builder builder = new RestAdapter.Builder()
                .setEndpoint("https://s3.amazonaws.com/jon-hancock-phunware/")
                .setClient(new OkClient(new OkHttpClient()));

        RestAdapter adtr = builder.build();

        StadiumService std = adtr.create(StadiumService.class);

        std.getStadiums(new Callback<List<Stadium>>() {
            @Override
            public void success(List<Stadium> stds, Response response) {
                stadiums = stds;
                Log.e("MyTag", stadiums.toString());
                do{
                    if(stadiums != null){

                        new android.os.Handler().postDelayed(
                                new Runnable() {
                                    public void run() {
                                        Log.e(this.getClass().getSimpleName(), "Advertisement " + stadiums.size());

                                        RecyclerView rc_stadiums = (RecyclerView) findViewById(R.id.rc_stadiums);
                                        rc_stadiums.setLayoutManager(new LinearLayoutManager(DashBoardActivity.this));
                                        // 3. create an adapter
                                        stdAdtr = new com.gerberraxon.stadiuminfoapp.Adapters.StadiumAdapter(stadiums);
                                        // 4. set adapter
                                        rc_stadiums.setAdapter(stdAdtr);
                                        // 5. set item animator to DefaultAnimator
                                        rc_stadiums.setItemAnimator(new DefaultItemAnimator());

                                    }
                                },
                                5000);
                    }
                }while(stadiums == null);

            }

            @Override
            public void failure(RetrofitError error) {
                Log.w(this.getClass().getSimpleName(), error.toString());
                Toast.makeText(DashBoardActivity.this, error.toString(), Toast.LENGTH_LONG).show();
            }
        });


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_dash_board, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    class StadiumAdapter extends ArrayAdapter<Stadium> {
        public StadiumAdapter(Context context, List<Stadium> std){
            super(context, R.layout.stadium_list_item, std);
        }

        public View getView(int position, View convertView, ViewGroup parent) {
            LayoutInflater inflater = LayoutInflater.from(getContext());
            View item;

            item = inflater.inflate(R.layout.stadium_list_item, null);

            TextView tv_name = (TextView) item.findViewById(R.id.tv_name
            );
            TextView tv_address = (TextView) item.findViewById(R.id.tv_address);

            tv_name.setText(stadiums.get(position).getName());
            tv_address.setText(stadiums.get(position).getAddress());

            return item;
        }
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        try{

            if(parent.getId() == R.id.rc_stadiums){

               /* Intent descriptionIntent = new Intent(parent.getContext(), movieDescriptionActivity.class);
                Stadium selected = (Stadium) parent.getItemAtPosition(position);
                startActivity(descriptionIntent);*/
            }

        }catch(Exception e){
            e.printStackTrace();
        }
    }

}
