package com.gerberraxon.stadiuminfoapp.ModelData;

/**
 * Created by graxon on 18/10/15.
 */

public class StadiumItemData {

    private String name;
    private String address;

    public StadiumItemData(String name,String address){
        this.name = name;
        this.address = address;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getName() {

        return name;
    }

    public String getAddress() {
        return address;
    }
}