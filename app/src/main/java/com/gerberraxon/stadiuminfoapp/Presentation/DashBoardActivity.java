package com.gerberraxon.stadiuminfoapp.Presentation;

import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.gerberraxon.stadiuminfoapp.Adapters.StadiumAdapter;
import com.gerberraxon.stadiuminfoapp.R;
import com.gerberraxon.stadiuminfoapp.Retrofit.StadiumService;
import com.gerberraxon.stadiuminfoapp.beans.Stadium;
import com.squareup.okhttp.OkHttpClient;

import java.util.List;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.OkClient;
import retrofit.client.Response;


public class DashBoardActivity extends ActionBarActivity implements ListView.OnItemClickListener{

    private static Stadium stdSelected = new Stadium();
    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    private List<Stadium> stadiums;
    private Toolbar mToolbar;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dash_board);

        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        mToolbar = (Toolbar)findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayShowHomeEnabled(true);


        RestAdapter.Builder builder = new RestAdapter.Builder()
                .setEndpoint("https://s3.amazonaws.com/jon-hancock-phunware/")
                .setClient(new OkClient(new OkHttpClient()));

        RestAdapter adtr = builder.build();

        StadiumService std = adtr.create(StadiumService.class);


        mRecyclerView = (RecyclerView) findViewById(R.id.rc_stadiums);
        mLayoutManager = new LinearLayoutManager(DashBoardActivity.this);
        mRecyclerView.setLayoutManager(mLayoutManager);

        std.getStadiums(new Callback<List<Stadium>>() {
            @Override
            public void success(List<Stadium> stds, Response response) {
                stadiums = stds;

                mAdapter = new StadiumAdapter(DashBoardActivity.this, stadiums);
                mRecyclerView.setAdapter(mAdapter);
                mRecyclerView.setItemAnimator(new DefaultItemAnimator());
            }

            @Override
            public void failure(RetrofitError error) {
                Log.w(this.getClass().getSimpleName(), error.toString());
                Toast.makeText(DashBoardActivity.this, error.toString(), Toast.LENGTH_LONG).show();
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_dash_board, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        try{

            if(parent.getId() == R.id.rc_stadiums){

                Intent descriptionIntent = new Intent(parent.getContext(), StadiumDetail.class);
                stdSelected = (Stadium) parent.getItemAtPosition(position);

                startActivity(descriptionIntent);
            }

        }catch(Exception e){
            e.printStackTrace();
        }
    }

    public static Stadium getStadiumselected(){
        return stdSelected;
    }

}