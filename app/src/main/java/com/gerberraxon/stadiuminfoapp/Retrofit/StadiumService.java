package com.gerberraxon.stadiuminfoapp.Retrofit;

import com.gerberraxon.stadiuminfoapp.beans.Stadium;

import java.util.List;

import retrofit.http.GET;
import retrofit.Callback;

/**
 * Created by graxon on 18/10/15.
 */
public interface StadiumService {

    @GET("/nflapi-static.json")
    void getStadiums(Callback<List<Stadium>> callback);
}
