package com.gerberraxon.stadiuminfoapp;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Handler;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.WindowManager;
import android.widget.TextView;

import com.gerberraxon.stadiuminfoapp.Presentation.DashBoardActivity;


public class SplashActivity extends ActionBarActivity {

    private static final long SPLASH_SCREEN_DELAY = 2000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        TextView customEmblem;

        customEmblem = (TextView) findViewById(R.id.emblem);
        Typeface font = Typeface.createFromAsset(getAssets(), "fonts/ostrich-sans/OstrichSans-Heavy.otf");

        customEmblem.setTypeface(font);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                Intent dash = new Intent(SplashActivity.this, DashBoardActivity.class);

                SplashActivity.this.startActivity(dash);

                finish();
            }
        }, SPLASH_SCREEN_DELAY);
    }
}
