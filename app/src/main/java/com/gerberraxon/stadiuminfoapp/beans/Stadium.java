package com.gerberraxon.stadiuminfoapp.beans;

import java.util.List;

/**
 * Created by graxon on 18/10/15.
 */
public class Stadium {
    private String zip;
    private String phone;
    private String ticket_link;
    private String pcode;
    private String city;
    private String id;
    private String tollpfreephone;
    private List<Schedule> schedules;
    private String address;
    private String image_source;
    private String description;
    private String name;
    private String longitude;
    private String latitude;


    public void setZip(String zip) {
        this.zip = zip;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public void setTicket_link(String ticket_link) {
        this.ticket_link = ticket_link;
    }

    public void setPcode(String pcode) {
        this.pcode = pcode;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public void setTollpfreephone(String tollpfreephone) {
        this.tollpfreephone = tollpfreephone;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setSchedules(List<Schedule> schedules) {
        this.schedules = schedules;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getName() {

        return name;
    }

    public String getDescription() {
        return description;
    }

    public String getLongitude() {
        return longitude;
    }

    public String getLatitude() {
        return latitude;
    }


    public String getZip() {

        return zip;
    }

    public String getPhone() {
        return phone;
    }

    public String getTicket_link() {
        return ticket_link;
    }

    public String getPcode() {
        return pcode;
    }

    public String getCity() {
        return city;
    }

    public String getId() {
        return id;
    }

    public String getTollpfreephone() {
        return tollpfreephone;
    }

    public List<Schedule> getSchedules() {
        return schedules;
    }

    public String getAddress() {
        return address;
    }

    public String getImage_source() {
        return image_source;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public void setImage_source(String image_source) {
        this.image_source = image_source;
    }

    public void setDescription(String description) {
        this.description = description;
    }

}
